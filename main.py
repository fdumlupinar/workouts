from bs4 import BeautifulSoup
import requests
import csv
import prettyprint as pprint

url = "https://www.crossfitlando.com/saturday-1-9/"
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15'}
# url = "https://www.google.com"
response = requests.get(url, headers=headers)
soup = BeautifulSoup(response.text, "html.parser")


paragraphs = soup.findAll("p")
for p in paragraphs:
    if "WOD" in p.get_text():
        print(p.get_text())
        print("Let's separate these wods\n")


